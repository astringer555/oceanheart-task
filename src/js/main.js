import $ from "jquery";

let navTop = $('._top'),
    navBottom = $('._bottom'),
    navList = $('.nav_list'),
    listToggler = $('.nav-mobilemenu_btn');

let jsScrollNavigation = () => {
    let scrolled = $(window).scrollTop();
    if ( (scrolled > 20 && scrolled) && window.innerWidth >= 1210) {
        navTop.addClass('js-menu-scrolled');
        navBottom.addClass('js-menu-into');
    } else {
        navTop.removeClass('js-menu-scrolled');
        navBottom.removeClass('js-menu-into');
    }
}

$(window).resize(function () {
    if (window.innerWidth >= 1210) {
        $(window).scroll(jsScrollNavigation);
        navTop.addClass('js-menu-scrolled');
        navBottom.addClass('js-menu-into');
        listToggler.removeClass('js-navlist-collapsed');
        $(navList).removeAttr('style')
    } else if (window.innerWidth < 1209) {
        navTop.removeClass('js-menu-scrolled');
        navBottom.removeClass('js-menu-into');
        listToggler.addClass('js-navlist-collapsed');
    }
})

if (window.innerWidth >= 1210) {
    $(window).scroll(jsScrollNavigation);
}

listToggler.click(function() {
    $(this).toggleClass('js-navlist-collapsed');
    $(navList).slideToggle({
        start: function() {
            $(this).css('display','flex');
        }
    })
})